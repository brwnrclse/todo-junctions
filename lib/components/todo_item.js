import React from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

/**
 * An individual todo, containing a status and content
 *
 * @param      {Object}           props
 * @param      {Object}           props.item       The item
 * @param      {Number}           props.index      Where the todo is in the list
 * @param      {Function}         props.todoStore  Functions for todo item
 *                                                      mutations
 *
 * @return     {React.Component}  A vdom representation of our component
 */
const TodoItem = ({item, index, todoStore}) => {
  const
    handleDelete = () => {
      todoStore.deleteTodo(index);
    },
    handleStatus = () => {
      todoStore.toggleTodo(index);
    },
    statusToID = (done) => {
      console.log(done);
      return done ? `finished` : `unfinished`;
    };

  return (
    <div className={`toju-TodoItem`}>
      <div className={`toju-TodoItem-status`}>
        <span className={`toju-TodoItem-status-btn`} id={statusToID(item.done)} onClick={handleStatus}>
        </span>
      </div>
      <div className={`toju-TodoItem-task`}>
        <div className={`toju-TodoItem-task-close`}>
          <span className={`toju-TodoItem-task-close-btn`} id={`close`} onClick={handleDelete}>
            <h3 className={`toju-TodoItem-task-close-btn-content`}>
              {`X`}
            </h3>
          </span>
        </div>
        <div className={`toju-TodoItem-task-body`}>
          <h2 className={`toju-TodoItem-task-body-content`} contentEditable={`true`}>
            {item.content}
          </h2>
        </div>
      </div>
    </div>
  );
};

TodoItem.displayName = `TodoItem`;
TodoItem.propTypes = {
  item: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  todoStore: PropTypes.object.isRequired
};

export default observer(TodoItem);
