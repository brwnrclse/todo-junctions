import React from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

/**
 * Display the name of the Toju
 *
 * @class      TodoBanner (name)
 * @param      {Object}  arg1        The argument 1
 * @param      {number}  arg1.title  The title
 * @return     {<type>}  { description_of_the_return_value }
 */
const TodoBanner = ({title}) => {
  return (
    <div className={`toju-TodoBanner`}>
      <h1>
        {`Toju: `}
      </h1>
      <h1 className={`toju-TodoBanner-title`}>
        {title}
      </h1>
    </div>
  );
};

TodoBanner.displayName = `TodoBanner`;
TodoBanner.propTypes = {
  title: PropTypes.string
};

export default observer(TodoBanner);
