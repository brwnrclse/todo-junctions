import React from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

import TodoItem from './todo_item.js';

/**
 * A list of todos
 *
 * @param      {Object}  props
 * @param      {Array}  props.todos      The todos
 * @param      {Object}  props.todoStore  The todo store
 *
 * @return     {React.Component}  A vdom representation of our component
 */
const TodoList = ({todos, todoStore}) => {
  return (
    <ul className={`toju-TodoList`}>
      {
        todos.map((item, index) => {
          return (
            <li className={`toju-TodoList-item`} key={Math.random()}>
              <TodoItem
                item={item}
                index={index}
                todoStore={todoStore}
              />
            </li>
          )
        })
      }
    </ul>
  );
};

TodoList.displayName = `TodoList`;
TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
  todoStore: PropTypes.object.isRequired
};

export default observer(TodoList);
