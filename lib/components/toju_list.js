import React from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

const TojuList = ({items}) => {
  return (
    <ul className={`toju-TojuList`}>
      {
        items.map((item) => {
          return (
            <li className={`toju-TojuList-item`} key={Math.random()}>
              <a className={`toju-TojuList-item-link`}>
                <h3 className={`toju-TojuList-item-title`}>
                  {item.title}
                </h3>
              </a>
            </li>
          )
        })
      }
    </ul>
  );
};

TojuList.displayName = `TojuList`;
TojuList.propTypes = {
  items: PropTypes.array
};

export default observer(TojuList);
