// External modules
import createHistory from 'history/createBrowserHistory';
import React from 'react';
import {render} from 'react-dom';
import {Router} from 'react-junctions';

// Our modules
import {AppJunction, AppScreen} from './screens/todo_screen.js';

// Constants
const HISTORY_STATE = createHistory();

HISTORY_STATE.listen((newAction, newLocation) => {
  console.log(`wver router: ${newLocation.pathname}`);
});

render(<Router
         history={HISTORY_STATE}
         junction={AppJunction}
         render={<AppScreen />}
       />, document.querySelector(`#toju-root`));
