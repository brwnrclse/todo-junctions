/* eslint arrow-body-style: 0 */
import {storiesOf} from '@kadira/storybook';
import React from 'react';

import {CenterDecorator} from '../utils/story-decorators';
import TodoItem from '../components/todo_item.js';
import TodoStore from '../stores/todo_store.js';

storiesOf(`TodoItem`, module)
  .addDecorator(CenterDecorator)
  .add(`Render single item`, () => {
    const fakeStore = TodoStore();

    return <TodoItem
              item={fakeStore.getTodo(0)}
              index={0}
              todoStore={fakeStore}
            />;
  });
