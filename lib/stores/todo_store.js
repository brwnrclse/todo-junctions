import {observable, autorun} from 'mobx';

/**
 * Create an observable (reactive) list of todos along with necessary functions
 *       for interaction. Also saves/loads the todos to localstorage if available.
 *
 * @class      TodoStore (name)
 * @return     {Object}  Our observable and all store functions
 */
const TodoStore = () => {
  const
    previousTodos = Array.isArray(localStorage.getItem(`toju-todos`)),
    todos = previousTodos ?
      observable(localStorage.getItem(`toju-todos`)) : observable([
        {
          content: `Finish todos`,
          done: false
        }
      ]),

    addTodo = (newTodo) => {
      todos.unshift(newTodo);
    },

    deleteTodo = (index) => {
      alert(`delete => ${todos[index].content}?`);
      todos.splice(index, 1);
    },

    getLength = () => {
      return todos.length - 1;
    },

    getTodo = (index) => {
      return todos[index];
    },

    toggleTodo = (index) => {
      todos[index].done = !todos[index].done;
      console.log(`Todo is now ${todos[index].done}`);
    };

  autorun(() => {
    localStorage.setItem(`toju-todos`, todos.peek());
  });

  return {
    addTodo,
    deleteTodo,
    getLength,
    getTodo,
    toggleTodo
  }
};

export default TodoStore;
