# todo-junctions
A simple todo app written with junctions

As a precursor to [eerf](https://bitbucket.org/brwnrclse/eerf) here's a simple todo app written with some cool stuff:

* [react](https://facebook.github.io/react/) => awesome-sauce views
* [mobx](https://mobxjs.github.io/mobx/) => awesome-sauce state management
* [junctions](https://junctions.js.org/) => awesome-sauce routing
* [react-storybook](https://getstorybook.io/) => awesome-sauce view testing

