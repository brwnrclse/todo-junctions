/**
 * Webpack config for storybooks
 */
module.exports = {
  module: {
    loaders: [
      {
        test: /\.styl$/,
        loaders: [`style-loader!css-loader!stylus-loader`, `raw`],
        include: `${__dirname}/../styles`
      }
    ]
  }
};
