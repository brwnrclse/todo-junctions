import {configure} from '@kadira/storybook';

import 'style!raw!css!../public/css/fix.css';
import 'style!raw!css!../public/css/typography.css';
import 'style!raw!stylus!../styles/main.styl';

const loadStories = () => {
  /* require all stories here */
  require(`../lib/stories/todo_item_story.js`);
};

configure(loadStories, module);
